package horbatiuk.library.repository;

import horbatiuk.library.entity.BookCopy;
import horbatiuk.library.entity.BookLoan;
import horbatiuk.library.entity.User;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface BookLoanRepository
        extends PagingAndSortingRepository<BookLoan, Integer> {

    List<BookLoan> findByBookCopyInAndReturnDateIsNull(List<BookCopy> copies);

    List<BookLoan> findByUser(User user);
}
