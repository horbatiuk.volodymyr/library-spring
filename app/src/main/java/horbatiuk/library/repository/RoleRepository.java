package horbatiuk.library.repository;

import horbatiuk.library.entity.Role;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface RoleRepository
        extends PagingAndSortingRepository<Role, Integer> {
}
