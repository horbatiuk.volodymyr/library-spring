package horbatiuk.library.repository;

import horbatiuk.library.entity.User;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface UserRepository
        extends PagingAndSortingRepository<User, Integer> {

    Optional<User> findByLogin(String login);
}
