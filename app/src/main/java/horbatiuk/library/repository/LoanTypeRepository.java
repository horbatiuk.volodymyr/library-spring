package horbatiuk.library.repository;

import horbatiuk.library.entity.LoanType;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface LoanTypeRepository
        extends PagingAndSortingRepository<LoanType, Integer> {

    Optional<LoanType> findByNameIgnoreCase(String name);
}
