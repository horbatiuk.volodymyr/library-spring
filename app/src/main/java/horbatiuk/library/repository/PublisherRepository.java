package horbatiuk.library.repository;

import horbatiuk.library.entity.Publisher;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PublisherRepository
        extends PagingAndSortingRepository<Publisher, Integer> {
}
