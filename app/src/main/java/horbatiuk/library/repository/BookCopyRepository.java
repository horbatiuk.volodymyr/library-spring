package horbatiuk.library.repository;

import horbatiuk.library.entity.Book;
import horbatiuk.library.entity.BookCopy;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface BookCopyRepository
        extends PagingAndSortingRepository<BookCopy, Integer> {

    List<BookCopy> findByBook(Book book);
}
