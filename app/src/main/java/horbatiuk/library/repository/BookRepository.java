package horbatiuk.library.repository;

import horbatiuk.library.entity.Book;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface BookRepository
        extends PagingAndSortingRepository<Book, Integer> {

    Page<Book> findByTitleContainingOrAuthorContaining(String partOfTitle, String partOfAuthor, Pageable pageable);
}
