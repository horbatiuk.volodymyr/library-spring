package horbatiuk.library.repository;

import horbatiuk.library.entity.BookRequest;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface BookRequestRepository
        extends PagingAndSortingRepository<BookRequest, Integer> {

    List<BookRequest> findBySatisfied(boolean satisfied);
}
