package horbatiuk.library.service;

import horbatiuk.library.entity.Book;
import horbatiuk.library.entity.BookCopy;
import horbatiuk.library.entity.BookLoan;
import horbatiuk.library.repository.BookCopyRepository;
import horbatiuk.library.repository.BookLoanRepository;
import horbatiuk.library.repository.BookRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class BookCopyService {

    private final BookRepository bookRepository;
    private final BookCopyRepository bookCopyRepository;
    private final BookLoanRepository bookLoanRepository;

    public BookCopyService(BookRepository bookRepository,
                           BookCopyRepository bookCopyRepository,
                           BookLoanRepository bookLoanRepository) {
        this.bookRepository = Objects.requireNonNull(bookRepository);
        this.bookCopyRepository = Objects.requireNonNull(bookCopyRepository);
        this.bookLoanRepository = Objects.requireNonNull(bookLoanRepository);
    }

    public List<BookCopy> findAllBy(int bookId) {
        Book book = findBookBy(bookId);
        return bookCopyRepository.findByBook(book);
    }

    private Book findBookBy(int bookId) {
        return bookRepository.findById(bookId)
                .orElseThrow(() -> new IllegalArgumentException("Cannot find book with id: " + bookId));
    }

    public List<BookCopy> findAvailable(int bookId) {
        Book book = findBookBy(bookId);
        List<BookCopy> allCopies = bookCopyRepository.findByBook(book);

        List<BookLoan> bookLoans
                = bookLoanRepository.findByBookCopyInAndReturnDateIsNull(allCopies);

        List<BookCopy> loanedCopies = bookLoans.stream()
                .map(BookLoan::getBookCopy)
                .collect(Collectors.toList());

        return allCopies.stream()
                .filter(bookCopy -> !loanedCopies.contains(bookCopy))
                .collect(Collectors.toList());
    }
}
