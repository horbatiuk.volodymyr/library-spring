package horbatiuk.library.service;

import horbatiuk.library.entity.User;
import horbatiuk.library.repository.UserRepository;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class UserService implements UserDetailsService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = Objects.requireNonNull(userRepository);
    }

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        User user = userRepository.findByLogin(login).orElseThrow(() ->
                new UsernameNotFoundException("No user found with login: " + login));

        String username = user.getLogin();
        String password = user.getPassword();
        boolean enabled = user.isEnabled();
        boolean accountNonExpired = user.isAccountNonExpired();
        boolean credentialsNonExpired = user.isCredentialsNonExpired();
        boolean accountNonLocked = user.isAccountNonLocked();
        Collection<? extends GrantedAuthority> authorities = user.getAuthorities();

        return new org.springframework.security.core.userdetails.User(
                username, password, enabled, accountNonExpired,
                credentialsNonExpired, accountNonLocked, authorities);
    }

    public Optional<User> getCurrentUser() {
        String name = getAuthentication().getName();
        if ("anonymousUser".equals(name)) {
            return Optional.empty();
        }
        return userRepository.findByLogin(name);
    }

    private Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    public User findById(int userId) {
        return userRepository.findById(userId)
                .orElseThrow(() -> new IllegalArgumentException("Cannot find user with id: " + userId));
    }

    public List<User> findAll() {
        return (List<User>) userRepository.findAll();
    }

    public User updateUserStatus(int userId, boolean enabled) {
        User user = findById(userId);
        user.setEnabled(enabled);
        return userRepository.save(user);
    }
}
