package horbatiuk.library.service;

import horbatiuk.library.controller.SortBy;
import horbatiuk.library.entity.Book;
import horbatiuk.library.repository.BookRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class BookSearchService {

    private final BookRepository bookRepository;

    public BookSearchService(BookRepository bookRepository) {
        this.bookRepository = Objects.requireNonNull(bookRepository);
    }

    public Page<Book> find(String query, SortBy sortBy, int pageSize, int requiredPage) {
        int page = requiredPage - 1;
        Sort sort = buildSort(sortBy);
        PageRequest pageRequest = PageRequest.of(page, pageSize, sort);
        if (StringUtils.isBlank(query)) {
            return bookRepository.findAll(pageRequest);
        } else {
            return bookRepository.findByTitleContainingOrAuthorContaining(query, query, pageRequest);
        }
    }

    private Sort buildSort(SortBy sortBy) {
        if (sortBy == SortBy.PUBLISHER) {
            return Sort.by("publisher.name").ascending();
        }
        return Sort.by(sortBy.getCode()).ascending();
    }
}
