package horbatiuk.library.service;

import horbatiuk.library.dto.BookLoanDto;
import horbatiuk.library.entity.BookCopy;
import horbatiuk.library.entity.BookLoan;
import horbatiuk.library.entity.User;
import horbatiuk.library.repository.BookLoanRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class BookLoanService {

    private final UserService userService;
    private final BookLoanRepository bookLoanRepository;
    private final int overdueFine;

    public BookLoanService(UserService userService, BookLoanRepository bookLoanRepository,
                           @Value("${library.overdue-fine}") int overdueFine) {
        this.userService = Objects.requireNonNull(userService);
        this.bookLoanRepository = Objects.requireNonNull(bookLoanRepository);
        if ((overdueFine < 1)) {
            throw new IllegalStateException("Overdue fine must be > 0, but in fact: " + overdueFine);
        }
        this.overdueFine = overdueFine;
    }

    public List<BookLoanDto> findLoaned(int userId) {
        User user = userService.findById(userId);
        List<BookLoan> bookLoans = bookLoanRepository.findByUser(user);
        return bookLoans.stream()
                .map(this::toBookLoanDto)
                .collect(Collectors.toList());
    }

    private BookLoanDto toBookLoanDto(BookLoan bookLoan) {
        BookLoanDto dto = new BookLoanDto();
        BookCopy bookCopy = bookLoan.getBookCopy();
        dto.bookTitle = bookCopy.getBook().getTitle();
        dto.bookAuthor = bookCopy.getBook().getAuthor();
        dto.loanType = bookLoan.getLoanType().getName();
        dto.bookInventoryNum = bookCopy.getInventoryNum();
        dto.issueDate = bookLoan.getIssueDate();
        LocalDate dueDate = bookLoan.getDueDate();
        dto.dueDate = dueDate;
        LocalDate returnDate = bookLoan.getReturnDate();
        dto.returnDate = returnDate;

        LocalDate dateForOverdueFineCalculation = returnDate;
        if (returnDate == null) {
            dateForOverdueFineCalculation = LocalDate.now();
        }
        if (dateForOverdueFineCalculation.isAfter(dueDate)) {
            long daysBetween = ChronoUnit.DAYS.between(dueDate, dateForOverdueFineCalculation);
            dto.totalOverdueFine = (int) (daysBetween * overdueFine);
        }
        return dto;
    }
}
