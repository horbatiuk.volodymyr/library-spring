package horbatiuk.library.service;

import horbatiuk.library.entity.*;
import horbatiuk.library.repository.BookLoanRepository;
import horbatiuk.library.repository.BookRepository;
import horbatiuk.library.repository.BookRequestRepository;
import horbatiuk.library.repository.LoanTypeRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

@Service
public class BookRequestService {

    private final UserService userService;
    private final BookRepository bookRepository;
    private final LoanTypeRepository loanTypeRepository;
    private final BookRequestRepository bookRequestRepository;
    private final BookCopyService bookCopyService;
    private final BookLoanRepository bookLoanRepository;
    private final int loanTermDays;

    public BookRequestService(UserService userService, BookRepository bookRepository,
                              LoanTypeRepository loanTypeRepository,
                              BookRequestRepository bookRequestRepository,
                              BookCopyService bookCopyService,
                              BookLoanRepository bookLoanRepository,
                              @Value("${library.loan-term-days}") int loanTermDays) {
        this.userService = Objects.requireNonNull(userService);
        this.bookRepository = Objects.requireNonNull(bookRepository);
        this.loanTypeRepository = Objects.requireNonNull(loanTypeRepository);
        this.bookRequestRepository = Objects.requireNonNull(bookRequestRepository);
        this.bookCopyService = Objects.requireNonNull(bookCopyService);
        this.bookLoanRepository = Objects.requireNonNull(bookLoanRepository);
        if ((loanTermDays < 1)) {
            throw new IllegalStateException("Loan term must be > 0, but in fact: " + loanTermDays);
        }
        this.loanTermDays = loanTermDays;
    }

    public BookRequest createRequest(int userId, int bookId, String loanTypeName) {
        BookRequest bookRequest = new BookRequest();
        User user = userService.findById(userId);
        bookRequest.setUser(user);

        Book book = bookRepository.findById(bookId).orElseThrow(() ->
                new IllegalArgumentException("Cannot find book with id: " + bookId));
        bookRequest.setBook(book);

        LoanType loanType = loanTypeRepository.findByNameIgnoreCase(loanTypeName).orElseThrow(() ->
                new IllegalArgumentException("Cannot find loanType with name: " + loanTypeName));
        bookRequest.setLoanType(loanType);

        return bookRequestRepository.save(bookRequest);
    }

    public BookRequest findById(int bookRequestId) {
        return bookRequestRepository.findById(bookRequestId).orElseThrow(() ->
                new IllegalArgumentException("Cannot find bookRequest with id: " + bookRequestId));
    }

    public List<BookRequest> findOpen() {
        return bookRequestRepository.findBySatisfied(false);
    }

    @Transactional // https://www.baeldung.com/transaction-configuration-with-jpa-and-spring
    public BookRequest satisfy(int requestId) {
        BookRequest bookRequest = findById(requestId);
        bookRequest.setSatisfied(true);

        BookLoan bookLoan = new BookLoan();
        int bookId = bookRequest.getBook().getId();
        bookLoan.setBookCopy(getAvailableCopyFor(bookId));
        bookLoan.setLoanType(bookRequest.getLoanType());
        bookLoan.setUser(bookRequest.getUser());
        bookLoan.setIssueDate(LocalDate.now());
        bookLoan.setDueDate(calculateDueDate(bookRequest.getLoanType()));
        bookLoanRepository.save(bookLoan);
        return bookRequestRepository.save(bookRequest);
    }

    private LocalDate calculateDueDate(LoanType loanType) {
        String loanTypeName = loanType.getName();
        if (StringUtils.isBlank(loanTypeName)) {
            throw new IllegalStateException("Loan type with id '"
                    + loanType.getId() + "' doesn't have non-empty name");
        }
        LocalDate now = LocalDate.now();
        switch (loanTypeName) {
            case "READING_ROOM":
                return now;
            case "HOME":
                return now.plusDays(loanTermDays);
            default:
                throw new IllegalStateException("Unknown loan type '" + loanTypeName
                        + "'. Only 'READING_ROOM' and 'HOME' are allowed.");
        }
    }

    private BookCopy getAvailableCopyFor(int bookId) {
        List<BookCopy> availableCopies = bookCopyService.findAvailable(bookId);
        return availableCopies.stream()
                .findFirst()
                .orElseThrow(() ->
                        new IllegalStateException("There are no available copies for bookId: " + bookId));
    }
}
