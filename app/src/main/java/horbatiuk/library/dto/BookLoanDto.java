package horbatiuk.library.dto;

import java.time.LocalDate;

public class BookLoanDto {

    public String bookTitle;
    public String bookAuthor;
    public String loanType;
    public String bookInventoryNum;
    public LocalDate issueDate;
    public LocalDate dueDate;
    public LocalDate returnDate;
    public int totalOverdueFine;
}
