package horbatiuk.library.entity.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.sql.Date;
import java.time.LocalDate;
import java.time.Year;

@Converter(autoApply = true)
public class YearConverter implements AttributeConverter<Year, Date> {

    @Override
    public Date convertToDatabaseColumn(Year year) {
        LocalDate localDate = LocalDate.of(year.getValue(), 1, 1);
        return Date.valueOf(localDate);
    }

    @Override
    public Year convertToEntityAttribute(Date date) {
        return Year.of(date.toLocalDate().getYear());
    }
}
