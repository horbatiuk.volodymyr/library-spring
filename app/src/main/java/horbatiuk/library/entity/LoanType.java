package horbatiuk.library.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
@Table(name = "loan_type")
public class LoanType extends BaseEntity {

    @Column(name = "name")
    @NotEmpty
    @Size(max = 25)
    private String name;

    @OneToMany(mappedBy = "loanType")
    private Set<BookLoan> bookLoans;

    @OneToMany(mappedBy = "loanType")
    private Set<BookRequest> bookRequests;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<BookLoan> getBookLoans() {
        return bookLoans;
    }

    public void setBookLoans(Set<BookLoan> bookLoans) {
        this.bookLoans = bookLoans;
    }

    public Set<BookRequest> getBookRequests() {
        return bookRequests;
    }

    public void setBookRequests(Set<BookRequest> bookRequests) {
        this.bookRequests = bookRequests;
    }

    @Override
    public String toString() {
        return name;
    }
}
