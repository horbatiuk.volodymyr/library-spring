package horbatiuk.library.entity;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "book_copy")
public class BookCopy extends BaseEntity {

    @ManyToOne(optional = false)
    @JoinColumn(name = "book_id", nullable = false)
    private Book book;

    @Column(name = "inventory_num")
    @NotEmpty
    @Size(min = 15, max = 15)
    private String inventoryNum;

    @OneToMany(mappedBy = "bookCopy")
    private Set<BookLoan> bookLoans;

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public String getInventoryNum() {
        return inventoryNum;
    }

    public void setInventoryNum(String inventoryNum) {
        this.inventoryNum = inventoryNum;
    }

    public Set<BookLoan> getBookLoans() {
        return bookLoans;
    }

    public void setBookLoans(Set<BookLoan> bookLoans) {
        this.bookLoans = bookLoans;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BookCopy bookCopy = (BookCopy) o;
        return Objects.equals(inventoryNum, bookCopy.inventoryNum);
    }

    @Override
    public int hashCode() {
        return Objects.hash(inventoryNum);
    }

    @Override
    public String toString() {
        return "BookCopy{" + book + "}";
    }
}
