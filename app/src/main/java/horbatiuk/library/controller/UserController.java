package horbatiuk.library.controller;

import horbatiuk.library.dto.BookLoanDto;
import horbatiuk.library.entity.BookRequest;
import horbatiuk.library.entity.User;
import horbatiuk.library.service.BookLoanService;
import horbatiuk.library.service.BookRequestService;
import horbatiuk.library.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@Controller
@RequestMapping("/users")
public class UserController {

    private static final String USER_ID = "user_id";

    private final UserService userService;
    private final BookRequestService bookRequestService;
    private final BookLoanService bookLoanService;

    public UserController(UserService userService,
                          BookRequestService bookRequestService,
                          BookLoanService bookLoanService) {
        this.userService = Objects.requireNonNull(userService);
        this.bookRequestService = Objects.requireNonNull(bookRequestService);
        this.bookLoanService = bookLoanService;
    }

    @ModelAttribute
    public void setCurrentUser(Model model) {
        User user = userService.getCurrentUser().orElseThrow(() ->
                new IllegalStateException("Current user is not authenticated"));
        model.addAttribute("user", user);
    }

    @PostMapping(path = "/{user_id}/requests")
    public String createRequest(@PathVariable(USER_ID) int userId,
                                @RequestParam("book_id") int bookId,
                                @RequestParam("loan_type_name") String loanTypeName, Model model) {
        BookRequest bookRequest = bookRequestService.createRequest(userId, bookId, loanTypeName);
        model.addAttribute("bookRequest", bookRequest);
        return "redirect:/users/" + userId + "/requests/" + bookRequest.getId();
    }

    @GetMapping(path = "/{user_id}/requests/{book_request_id}")
    public String userRequest(@PathVariable(USER_ID) int userId,
                              @PathVariable("book_request_id") int bookRequestId, Model model) {
        BookRequest bookRequest = bookRequestService.findById(bookRequestId);
        model.addAttribute("bookRequest", bookRequest);
        return "users/request";
    }


    @GetMapping("/{user_id}")
    public String index(@PathVariable("user_id") int userId, Model model) {
        List<BookLoanDto> bookLoans = bookLoanService.findLoaned(userId);
        model.addAttribute("bookLoans", bookLoans);
        return "users/index";
    }
}
