package horbatiuk.library.controller.converter;

import horbatiuk.library.controller.SortBy;
import org.springframework.core.convert.converter.Converter;

public class StringToSortByConverter implements Converter<String, SortBy> {

    @Override
    public SortBy convert(String code) {
        try {
            return SortBy.valueOf(code.toUpperCase());
        } catch (IllegalArgumentException e) {
            return null;
        }
    }
}
