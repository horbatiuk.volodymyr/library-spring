package horbatiuk.library.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class SecurityController {

    private static final String LOGIN_PAGE = "login";

    @RequestMapping("/login")
    public String login() {
        return LOGIN_PAGE;
    }

    @RequestMapping("/login-error")
    public String loginError(Model model) {
        model.addAttribute("loginError", true);
        return LOGIN_PAGE;
    }
}
