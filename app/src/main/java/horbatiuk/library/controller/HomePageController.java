package horbatiuk.library.controller;

import horbatiuk.library.entity.Book;
import horbatiuk.library.service.BookSearchService;
import horbatiuk.library.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
public class HomePageController {

    private static final Logger LOG = LoggerFactory.getLogger(HomePageController.class);
    private static final String EMPTY_STRING = "";
    private static final String QUERY = "query";
    private static final String SORT_BY = "sort_by";
    private static final String PAGE_SIZE = "page_size";
    private static final String REQUIRED_PAGE = "required_page";
    private static final String SEARCH_PARAMS = "searchParams";
    private static final String PAGE_NUMBERS = "page_numbers";

    private final BookSearchService bookSearchService;
    private final UserService userService;
    private final SortBy sortByDefault;
    private final int pageSizeDefault;

    public HomePageController(BookSearchService bookSearchService, UserService userService,
                              @Value("${library.sort-by}") SortBy sortByDefault,
                              @Value("${library.page-size}") int pageSizeDefault) {
        this.bookSearchService = Objects.requireNonNull(bookSearchService);
        this.userService = Objects.requireNonNull(userService);
        this.sortByDefault = Objects.requireNonNull(sortByDefault);
        this.pageSizeDefault = pageSizeDefault;
    }

    @ModelAttribute
    public void setCurrentUser(Model model) {
        userService.getCurrentUser()
                .ifPresent(user -> model.addAttribute("user", user));
    }

    @GetMapping(path = {"/", "/index"})
    public String index(@RequestParam(QUERY) Optional<String> queryOptional,
                        @RequestParam(SORT_BY) Optional<SortBy> sortByOptional,
                        @RequestParam(PAGE_SIZE) Optional<Integer> pageSizeOptional,
                        @RequestParam(REQUIRED_PAGE) Optional<Integer> requiredPageOptional,
                        Model model) {
        Map<String, Object> searchParams = new HashMap<>();
        String query = queryOptional.orElse(EMPTY_STRING);
        searchParams.put(QUERY, query);
        SortBy sortBy = sortByOptional.orElse(sortByDefault);
        searchParams.put(SORT_BY, sortBy);
        Integer pageSize = pageSizeOptional.orElse(pageSizeDefault);
        searchParams.put(PAGE_SIZE, pageSize);
        Integer requiredPage = requiredPageOptional.orElse(1);
        searchParams.put(REQUIRED_PAGE, requiredPage);

        Page<Book> bookPage = bookSearchService.find(query, sortBy, pageSize, requiredPage);
        List<Book> books = bookPage.getContent();
        model.addAttribute("books", books);
        LOG.info("Books from database: {}", books);

        int totalPages = bookPage.getTotalPages();
        List<Integer> pageNumbers = Collections.emptyList();
        if (totalPages > 0) {
            pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
        }
        searchParams.put(PAGE_NUMBERS, pageNumbers);
        model.addAttribute(SEARCH_PARAMS, searchParams);
        return "index";
    }

    @GetMapping("/simulate-exception")
    public void simulateError() {
        throw new RuntimeException("I intentionally threw this exception");
    }
}
