package horbatiuk.library.controller;

import horbatiuk.library.entity.BookCopy;
import horbatiuk.library.entity.User;
import horbatiuk.library.service.BookCopyService;
import horbatiuk.library.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@Controller
@RequestMapping("/books")
public class BookController {

    private final BookCopyService bookCopyService;
    private final UserService userService;

    public BookController(BookCopyService bookCopyService, UserService userService) {
        this.bookCopyService = Objects.requireNonNull(bookCopyService);
        this.userService = Objects.requireNonNull(userService);
    }

    @ModelAttribute
    public void setCurrentUser(Model model) {
        User user = userService.getCurrentUser().orElseThrow(() ->
                new IllegalStateException("Current user is not authenticated"));
        model.addAttribute("user", user);
    }

    @GetMapping(path = "/{book_id}/copies")
    public String getAvailableCopies(@PathVariable("book_id") int bookId,
                                     @RequestParam(name = "available", required = false) boolean available,
                                     Model model) {
        List<BookCopy> bookCopies;
        if (available) {
            bookCopies = bookCopyService.findAvailable(bookId);
        } else {
            bookCopies = bookCopyService.findAllBy(bookId);
        }
        model.addAttribute("bookCopies", bookCopies);
        return "books/copies";
    }
}
