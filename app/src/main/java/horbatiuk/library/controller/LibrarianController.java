package horbatiuk.library.controller;

import horbatiuk.library.entity.BookRequest;
import horbatiuk.library.entity.User;
import horbatiuk.library.service.BookRequestService;
import horbatiuk.library.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@Controller
@RequestMapping("/librarian")
public class LibrarianController {

    private final UserService userService;
    private final BookRequestService bookRequestService;

    public LibrarianController(UserService userService, BookRequestService bookRequestService) {
        this.userService = Objects.requireNonNull(userService);
        this.bookRequestService = Objects.requireNonNull(bookRequestService);
    }

    @ModelAttribute
    public void setCurrentUser(Model model) {
        User user = userService.getCurrentUser().orElseThrow(() ->
                new IllegalStateException("Current user is not authenticated"));
        model.addAttribute("user", user);
    }

    @GetMapping(path = {"", "index"})
    public String index(Model model) {
        List<BookRequest> openRequests = bookRequestService.findOpen();
        model.addAttribute("openRequests", openRequests);
        return "librarian/index";
    }

    @PostMapping
    public String satisfyRequest(@RequestParam("request_id") int requestId) {
        bookRequestService.satisfy(requestId);
        return "redirect:/librarian/index";
    }
}
