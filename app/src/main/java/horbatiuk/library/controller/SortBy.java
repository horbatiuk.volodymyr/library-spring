package horbatiuk.library.controller;

public enum SortBy {
    TITLE("title"), AUTHOR("author"), PUBLISHER("publisher"), YEAR("year");

    private final String code;

    SortBy(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
