package horbatiuk.library.controller;

import horbatiuk.library.entity.User;
import horbatiuk.library.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@Controller
@RequestMapping("/admin")
public class AdminController {

    private static final Logger LOG = LoggerFactory.getLogger(AdminController.class);

    private final UserService userService;

    public AdminController(UserService userService) {
        this.userService = Objects.requireNonNull(userService);
    }

    @ModelAttribute
    public void setCurrentUser(Model model) {
        User user = userService.getCurrentUser().orElseThrow(() ->
                new IllegalStateException("Current user is not authenticated"));
        model.addAttribute("user", user);
    }

    @GetMapping(path = {"", "index"})
    public String index(Model model) {
        List<User> users = userService.findAll();
        model.addAttribute("users", users);
        return "admin/index";
    }

    @PostMapping()
    public String updateUserStatus(@RequestParam("user_id") int userId,
                                   @RequestParam("enabled") boolean enabled) {
        User user = userService.updateUserStatus(userId, enabled);
        LOG.info("User '{} {}' got 'enabled' flag as '{}'",
                user.getFirstName(), user.getLastName(), enabled);
        return "redirect:/admin/index";
    }
}
