package horbatiuk.library.controller;

import horbatiuk.library.entity.BookRequest;
import horbatiuk.library.entity.User;
import horbatiuk.library.service.BookRequestService;
import horbatiuk.library.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.ui.Model;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class LibrarianControllerTest {

    private static final User USER = new User();

    @Mock
    private UserService userService;
    @Mock
    private BookRequestService bookRequestService;
    @Mock
    private Model model;

    private LibrarianController controller;
    private String pageName;

    @BeforeEach
    void setUp() {
        controller = new LibrarianController(userService, bookRequestService);
    }

    @Test
    void shouldThrowWhenUserServiceIsNull() {
        assertThrows(NullPointerException.class, () ->
                new LibrarianController(null, bookRequestService));
    }

    @Test
    void shouldThrowWhenBookRequestServiceIsNull() {
        assertThrows(NullPointerException.class, () ->
                new LibrarianController(userService, null));
    }

    @Test
    void shouldThrowWhenTryingToSetCurrentUserButTheUserIsNotAuthenticated() {
        Throwable exception = assertThrows(IllegalStateException.class, () ->
                controller.setCurrentUser(model));
        assertThat(exception.getMessage()).isEqualTo("Current user is not authenticated");
        verifyNoInteractions(model);
    }

    @Test
    void shouldSetCurrentUser() {
        when(userService.getCurrentUser()).thenReturn(Optional.of(USER));

        controller.setCurrentUser(model);

        verify(model).addAttribute("user", USER);
    }

    @Test
    void shouldReturnIndexPageWithOpenedBookRequests() {
        BookRequest bookRequest = new BookRequest();
        List<BookRequest> openRequests = Collections.singletonList(bookRequest);
        when(bookRequestService.findOpen()).thenReturn(openRequests);

        pageName = controller.index(model);

        verify(model).addAttribute("openRequests", openRequests);
        assertThat(pageName).isEqualTo("librarian/index");
    }

    @Test
    void shouldSatisfyRequest() {
        int requestId = 1;

        pageName = controller.satisfyRequest(requestId);

        verify(bookRequestService).satisfy(requestId);
        assertThat(pageName).isEqualTo("redirect:/librarian/index");
    }
}
