package horbatiuk.library.controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.ui.Model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class SecurityControllerTest {

    private static final String LOGIN_PAGE = "login";

    @Mock
    private Model model;

    private SecurityController controller = new SecurityController();

    @Test
    void shouldReturnLoginPage() {
        assertThat(controller.login()).isEqualTo(LOGIN_PAGE);
    }

    @Test
    void shouldAddLoginErrorAttribute() {
        String pageName = controller.loginError(model);

        verify(model).addAttribute("loginError", true);
        assertThat(pageName).isEqualTo(LOGIN_PAGE);
    }
}
