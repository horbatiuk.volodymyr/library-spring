package horbatiuk.library.controller;

import horbatiuk.library.entity.BookCopy;
import horbatiuk.library.entity.User;
import horbatiuk.library.service.BookCopyService;
import horbatiuk.library.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.ui.Model;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class BookControllerTest {

    private static final User USER = new User();
    private static final int BOOK_ID = 1;
    private static final BookCopy BOOK_COPY = new BookCopy();
    private static final List<BookCopy> BOOK_COPIES = Collections.singletonList(BOOK_COPY);
    private static final String BOOK_COPIES_ATTRIBUTE_NAME = "bookCopies";
    private static final String BOOK_COPIES_PAGE = "books/copies";

    @Mock
    private BookCopyService bookCopyService;
    @Mock
    private UserService userService;
    @Mock
    private Model model;

    private BookController controller;
    private boolean available;
    private String pageName;

    @BeforeEach
    void setUp() {
        controller = new BookController(bookCopyService, userService);
    }

    @Test
    void shouldThrowWhenBookCopyServiceIsNull() {
        assertThrows(NullPointerException.class, () -> new BookController(null, userService));
    }

    @Test
    void shouldThrowWhenUserServiceIsNull() {
        assertThrows(NullPointerException.class, () -> new BookController(bookCopyService, null));
    }

    @Test
    void shouldThrowWhenTryingToSetCurrentUserButTheUserIsNotAuthenticated() {
        Throwable exception = assertThrows(IllegalStateException.class, () -> controller.setCurrentUser(model));
        assertThat(exception.getMessage()).isEqualTo("Current user is not authenticated");
        verifyNoInteractions(model);
    }

    @Test
    void shouldSetCurrentUser() {
        when(userService.getCurrentUser()).thenReturn(Optional.of(USER));

        controller.setCurrentUser(model);

        verify(model).addAttribute("user", USER);
    }

    @Test
    void shouldReturnAllCopies() {
        when(bookCopyService.findAllBy(BOOK_ID)).thenReturn(BOOK_COPIES);
        available = false;

        pageName = controller.getAvailableCopies(BOOK_ID, available, model);

        verify(model).addAttribute(BOOK_COPIES_ATTRIBUTE_NAME, BOOK_COPIES);
        assertThat(pageName).isEqualTo(BOOK_COPIES_PAGE);
    }

    @Test
    void shouldReturnOnlyAvailableCopies() {
        when(bookCopyService.findAvailable(BOOK_ID)).thenReturn(BOOK_COPIES);
        available = true;

        pageName = controller.getAvailableCopies(BOOK_ID, available, model);

        verify(model).addAttribute(BOOK_COPIES_ATTRIBUTE_NAME, BOOK_COPIES);
        assertThat(pageName).isEqualTo(BOOK_COPIES_PAGE);
    }
}
