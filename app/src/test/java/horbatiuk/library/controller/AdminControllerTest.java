package horbatiuk.library.controller;

import horbatiuk.library.entity.User;
import horbatiuk.library.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.ui.Model;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class AdminControllerTest {

    private static final User USER = new User();

    @Mock
    private UserService userService;
    @Mock
    private Model model;

    private AdminController controller;
    private String pageName;

    @BeforeEach
    void setUp() {
        controller = new AdminController(userService);
    }

    @Test
    void shouldThrowWhenUserServiceIsNull() {
        assertThrows(NullPointerException.class, () -> new AdminController(null));
    }

    @Test
    void shouldThrowWhenTryingToSetCurrentUserButTheUserIsNotAuthenticated() {
        Throwable exception = assertThrows(IllegalStateException.class, () ->
                controller.setCurrentUser(model));
        assertThat(exception.getMessage()).isEqualTo("Current user is not authenticated");
        verifyNoInteractions(model);
    }

    @Test
    void shouldSetCurrentUser() {
        when(userService.getCurrentUser()).thenReturn(Optional.of(USER));

        controller.setCurrentUser(model);

        verify(model).addAttribute("user", USER);
    }

    @Test
    void shouldReturnIndexPageWithUsers() {
        List<User> users = Collections.singletonList(USER);
        when(userService.findAll()).thenReturn(users);

        pageName = controller.index(model);

        assertThat(pageName).isEqualTo("admin/index");
        verify(model).addAttribute("users", users);
    }

    @Test
    void shouldUpdateUserStatus() {
        int userId = 1;
        boolean enabled = true;
        when(userService.updateUserStatus(userId, enabled)).thenReturn(USER);

        pageName = controller.updateUserStatus(userId, enabled);

        verify(userService).updateUserStatus(userId, enabled);
        assertThat(pageName).isEqualTo("redirect:/admin/index");
    }
}
