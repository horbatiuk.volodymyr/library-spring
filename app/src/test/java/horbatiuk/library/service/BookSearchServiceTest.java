package horbatiuk.library.service;

import horbatiuk.library.controller.SortBy;
import horbatiuk.library.entity.Book;
import horbatiuk.library.repository.BookRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class BookSearchServiceTest {

    private static final int PAGE_SIZE = 5;
    private static final int REQUIRED_PAGE = 1;

    @Mock
    private BookRepository bookRepository;
    @Mock
    private Page<Book> pageWithBooks;

    private BookSearchService service;
    private String query;
    private Page<Book> actualPage;

    @BeforeEach
    void setUp() {
        service = new BookSearchService(bookRepository);
    }

    @Test
    void shouldThrowWhenBookRepositoryIsNull() {
        assertThrows(NullPointerException.class, () -> new BookSearchService(null));
    }

    @Test
    void shouldReturnPageWithBooksWhenQueryIsNullAndSortIsByTitle() {
        Sort sort = Sort.by(SortBy.TITLE.getCode()).ascending();
        Pageable pageRequest = PageRequest.of(REQUIRED_PAGE - 1, PAGE_SIZE, sort);
        when(bookRepository.findAll(pageRequest)).thenReturn(pageWithBooks);

        actualPage = service.find(null, SortBy.TITLE, PAGE_SIZE, REQUIRED_PAGE);

        assertThat(actualPage).isEqualTo(pageWithBooks);
    }


    @Test
    void shouldReturnPageWithBooksWhenQueryIsEmptyAndSortIsByPublisher() {
        Sort sort = Sort.by("publisher.name").ascending();
        Pageable pageRequest = PageRequest.of(REQUIRED_PAGE - 1, PAGE_SIZE, sort);
        when(bookRepository.findAll(pageRequest)).thenReturn(pageWithBooks);

        actualPage = service.find(" ", SortBy.PUBLISHER, PAGE_SIZE, REQUIRED_PAGE);

        assertThat(actualPage).isEqualTo(pageWithBooks);
    }

    @Test
    void shouldReturnPageWithBooksWhenQueryIsNotEmptyAndSortIsByAuthor() {
        String query = "Java";
        Sort sort = Sort.by(SortBy.AUTHOR.getCode()).ascending();
        Pageable pageRequest = PageRequest.of(REQUIRED_PAGE - 1, PAGE_SIZE, sort);
        when(bookRepository.findByTitleContainingOrAuthorContaining(query, query, pageRequest))
                .thenReturn(pageWithBooks);

        actualPage = service.find(query, SortBy.AUTHOR, PAGE_SIZE, REQUIRED_PAGE);

        assertThat(actualPage).isEqualTo(pageWithBooks);
    }
}
