package horbatiuk.library.service;

import horbatiuk.library.repository.BookLoanRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
class BookLoanServiceTest {

    private static final int OVERDUE_FINE = 3;

    @Mock
    private UserService userService;
    @Mock
    private BookLoanRepository bookLoanRepository;

    private BookLoanService service;

    @BeforeEach
    void setUp() {
        service = new BookLoanService(userService, bookLoanRepository, OVERDUE_FINE);
    }

    @Test
    void shouldThrowWhenUserServiceIsNull() {
        assertThrows(NullPointerException.class, () ->
                new BookLoanService(null, bookLoanRepository, OVERDUE_FINE));
    }

    @Test
    void shouldThrowWhenBookLoanRepositoryIsNull() {
        assertThrows(NullPointerException.class, () ->
                new BookLoanService(userService, null, OVERDUE_FINE));
    }

    @Test
    void shouldThrowWhenOverdueFineIsLessThanOne() {
        int overdueFine = 0;

        Throwable exception = assertThrows(IllegalStateException.class, () ->
                new BookLoanService(userService, bookLoanRepository, overdueFine));
        assertThat(exception.getMessage())
                .isEqualTo("Overdue fine must be > 0, but in fact: " + overdueFine);
    }
}
