package horbatiuk.library.service;

import horbatiuk.library.entity.Book;
import horbatiuk.library.entity.BookCopy;
import horbatiuk.library.repository.BookCopyRepository;
import horbatiuk.library.repository.BookLoanRepository;
import horbatiuk.library.repository.BookRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class BookCopyServiceTest {

    private static final int BOOK_ID = 1;
    private static final Book BOOK = new Book();

    @Mock
    private BookRepository bookRepository;
    @Mock
    private BookCopyRepository bookCopyRepository;
    @Mock
    private BookLoanRepository bookLoanRepository;

    private BookCopyService service;

    @BeforeEach
    void setUp() {
        service = new BookCopyService(bookRepository, bookCopyRepository, bookLoanRepository);
    }

    @Test
    void shouldThrowWhenBookRepositoryIsNull() {
        assertThrows(NullPointerException.class, () ->
                new BookCopyService(null, bookCopyRepository, bookLoanRepository));
    }

    @Test
    void shouldThrowWhenBookCopyRepositoryIsNull() {
        assertThrows(NullPointerException.class, () ->
                new BookCopyService(bookRepository, null, bookLoanRepository));
    }

    @Test
    void shouldThrowWhenBookLoanRepositoryIsNull() {
        assertThrows(NullPointerException.class, () ->
                new BookCopyService(bookRepository, bookCopyRepository, null));
    }

    @Test
    void shouldThrowWhenBookRepositoryDoesNotContainBookWithGivenId() {
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> service.findAllBy(BOOK_ID));
        assertThat(exception.getMessage()).isEqualTo("Cannot find book with id: " + BOOK_ID);
    }

    @Test
    void shouldReturnBookCopiesForGivenBookId() {
        when(bookRepository.findById(BOOK_ID)).thenReturn(Optional.of(BOOK));
        List<BookCopy> bookCopies = Collections.singletonList(new BookCopy());
        when(bookCopyRepository.findByBook(BOOK)).thenReturn(bookCopies);

        List<BookCopy> actualBookCopies = service.findAllBy(BOOK_ID);

        assertThat(actualBookCopies).isEqualTo(bookCopies);
    }
}
